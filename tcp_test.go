package gnetwork

import (
	"context"
	"fmt"
	"net"
	"os"
	"os/signal"
	"testing"
)

type tcpHandle struct {
	DefaultEvHandle
}

func (self *tcpHandle) OnFinish(id int32) {
	fmt.Printf("处理协程 %d 结束.\n", id)
}

func (self *tcpHandle) OnAccept(connId int32, conn net.Conn) {
}

func (self *tcpHandle) OnError(err error) {
	fmt.Println(err)
}

func (self *tcpHandle) OnReceived(connId int32, content []byte) error {
	fmt.Printf("%d, %s\n", len(content), string(content[0:len(content)]))

	return nil
}

func (self *tcpHandle) OnClosed(connId int32) {
	fmt.Println(connId, "关闭了!")
}

func TestNewTcpServe(t *testing.T) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	net, err := net.Listen("tcp", "127.0.0.1:5555")
	if err != nil {
		panic(err)
	}
	var tcpHandle tcpHandle

	s := NewTcpServe(context.Background(), net, &tcpHandle, NewEvLoopQueOp(2048))
	s.Launch()
	for i := 1; i < 256; i++ {
		s.Launch()
	}

	s.Run()

	select {
	case <-c:
		s.StopAndWait()
	}
	fmt.Println("is over!")
}