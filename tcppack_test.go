package gnetwork

import (
	"context"
	"fmt"
	"net"
	"os"
	"os/signal"
	"testing"
)

type tcpPackHandle struct {
	DefaultEvHandle
	s *TcpPackServe
}

type Conditioner struct {
}

func (self *Conditioner) ParserPackHeader(content []byte) (l int, err error) {
	return ParserPackHeader(content)
}

func (self *Conditioner) BuildPackData(data []byte) []byte {
	return BuildPackData(data)
}

func (self *tcpPackHandle) OnCmdHandle(id int32, evCmd int32) {
	fmt.Printf("处理协程%d, 处理事件指令 %v\n", id, evCmd)
}

func (self *tcpPackHandle) OnFinish(id int32) {
	fmt.Printf("处理协程 %d 结束.\n", id)
}

func (self *tcpPackHandle) OnAccept(connId int32, conn net.Conn) {
	self.s.SendData(connId, []byte("recive success!"))
}

func (self *tcpPackHandle) OnError(err error) {
	fmt.Println(err)
}

func (self *tcpPackHandle) OnReceived(connId int32, content []byte) error {
	fmt.Printf("%d, %s\n", len(content), string(content))

	if self.s != nil {
		self.s.SendData(connId, content)
	}

	return nil
}

func (self *tcpPackHandle) OnClosed(connId int32) {
	fmt.Println(connId, "关闭了!")
}

func TestNewTcpPackServe(t *testing.T) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	listener, err := net.Listen("tcp", "127.0.0.1:5555")
	if err != nil {
		panic(err)
	}
	var cd Conditioner
	var tcpPackHandle tcpPackHandle
	tcp := NewTcpServe(context.Background(), listener, &tcpPackHandle, NewEvLoopQueOp(2048))
	tcpPackHandle.s = NewTcpPackServe(tcp, PackHeaderSize, &cd)
	tcpPackHandle.s.Launch()
	for i := 1; i < 256; i++ {
		tcpPackHandle.s.Launch()
	}

	tcpPackHandle.s.Run()

	select {
	case <-c:
		tcpPackHandle.s.StopAndWait()
	}
	fmt.Println("is over!")
}