package gnetwork

import "encoding/binary"

func init() {
	var defaultPackHeader PackHeader
	PackHeaderSize = binary.Size(defaultPackHeader)
}

var PackHeaderSize int

type PackHeader struct {
	Flag int32
}
