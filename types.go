package gnetwork

import (
	"net"
	"sync"
)

const (
	defaultBufSize = 4096
)

var connNum int32 = 0

type connType struct {
	connId int32
	conn   net.Conn
}

type wConnType struct {
	connType
	data []byte
}

type connItem struct {
	conn net.Conn
	wMtx sync.Mutex
}

const (
	accept = -1
	read   = -2
	write  = -3
	close  = -4
)

type Control interface {
	Launch() int32
	Run()
	StopAndWait()
}

type ErrorHandle interface {
	OnError(err error)
}

type IOHandle interface {
	rHandle(connId int32)
	wHandle(connId int32, data []byte)
}

type ITcpHandle interface {
	ErrorHandle
	OnAccept(connId int32, conn net.Conn)
	//OnReceived 对于一个connect来说是同步的
	OnReceived(connId int32, content []byte) error
	OnClosed(connId int32)
	OnFinish(id int32)
}

type ITcpPackRegulator interface {
	ParserPackHeader(content []byte) (l int, err error)
	BuildPackData(data []byte) []byte
}

type DefaultEvHandle struct {
}

func (self *DefaultEvHandle) OnReceived(connId int32, content []byte) error {
	return nil
}

func (self *DefaultEvHandle) OnClosed(connId int32) {
}

func (self *DefaultEvHandle) OnFinish(id int32) {
}

func (self *DefaultEvHandle) OnAccept(connId int32, conn net.Conn) {
}

func (self *DefaultEvHandle) OnError(err error) {
}
