package gnetwork

import (
	"reflect"
	"testing"
)

func TestBuildPackData(t *testing.T) {
	type args struct {
		data []byte
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		// TODO: Add test cases.
		{
			name: "1",
			args: args{
				data: []byte("qweqwe"),
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BuildPackData(tt.args.data); reflect.DeepEqual(got, tt.want) {
				t.Errorf("BuildPackData() = %v, want %v", got, tt.want)
			}
		})
	}
}
