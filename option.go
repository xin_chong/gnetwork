package gnetwork

type Option interface {
	GetValue() interface{}
}

type EvLoopQueOp struct {
	queSize uint32
}

func NewEvLoopQueOp(queSize uint32) *EvLoopQueOp {
	return &EvLoopQueOp{queSize: queSize}
}

func (self EvLoopQueOp) GetValue() interface{} {
	return self.queSize
}
